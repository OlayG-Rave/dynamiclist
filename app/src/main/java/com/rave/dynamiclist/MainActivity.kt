package com.rave.dynamiclist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.rave.dynamiclist.databinding.ActivityMainBinding

/**
 * Step 1: Add RecyclerView to the Layout
 * Step 2: Setup RecyclerView LayoutManager
 * Step 3: Setup RecyclerView Adapter
 * Step 4: Connect RecyclerView to Custom Adapter
 * Step 5: Setting up click listener so we can pass data clicked to view
 */
class MainActivity : AppCompatActivity() {

    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val fakeContacts = listOf(
        "kshdf", "sadfdsf", "oiuhtrij", "oijijore",
        "kshdf", "sadfdsf", "oiuhtrij", "oijijore",
        "kshdf", "sadfdsf", "oiuhtrij", "oijijore",
        "kshdf", "sadfdsf", "oiuhtrij", "oijijore",
        "kshdf", "sadfdsf", "oiuhtrij", "oijijore",
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.rvList.layoutManager = LinearLayoutManager(this)
        // There are 2 main ways to satisfy the higher order function param
        //1)  We can do it inline similar to observer
                /*ContactsAdapter { contact ->

                }*/
        //2)  We can pass the method reference which has same signature as our higher order function
        binding.rvList.adapter = ContactsAdapter(::contactClicked).apply { addContacts(fakeContacts) }
    }

    private fun contactClicked(contact: String) {
        Toast.makeText(this, contact, Toast.LENGTH_SHORT).show()
    }
}