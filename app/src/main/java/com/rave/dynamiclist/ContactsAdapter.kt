package com.rave.dynamiclist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rave.dynamiclist.databinding.ItemContactBinding

/**
 * Step 1: Extend RecyclerView.Adapter
 * Step 2: Create/Setup Custom ViewHolder and Pass into RecyclerView.Adapter
 * Step 3: Override required methods for Adapter onCreateViewHolder, onBindViewHolder, getItemCount
 * Step 4: Setup your list of data to display
 */
class ContactsAdapter(
    // We are using a high order function to pass the data to the view(fragment) once the contact item is clicked
    private val contactClicked: (String) -> Unit
) : RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>() {

    private var contacts = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        val binding = ItemContactBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ContactsViewHolder(binding).apply {
            // itemView is a reference to the root of the view we inflated using binding
            // So we are setting a click listener on the whole view
            itemView.setOnClickListener {
                // We get the contact that user selected
                val contact = contacts[adapterPosition]
                // Passing the contact clicked to our higher order function
                contactClicked(contact)
            }
        }
    }

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        val contact = contacts[position]
        holder.loadContact(contact)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    fun addContacts(contacts: List<String>) {
        this.contacts = contacts.toMutableList()
        notifyDataSetChanged()
    }

    class ContactsViewHolder(
        private val binding: ItemContactBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadContact(name: String) {
            binding.tvName.text = name
        }
    }
}